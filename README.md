## Hi, I'm Mitch :wave:

I'm currently living in Kansas with my wife Bri and my Australian Shepherd Macie.

### What I do here

I'm a Backend Engineer on the [Distribution:Deploy team](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/#team-members).

I work primarily on Cloud-Native projects, including the GitLab [Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator) and [Helm charts](https://gitlab.com/gitlab-org/charts/gitlab).

### Some things I've made

Here are some personal projects I've made while working here:

- [1-on-1 report bot](https://gitlab.com/mnielsen/1on1-report-bot): Automatically creates a work report as a GitLab snippet for weekly manager reviews.
- [issue-bot](https://gitlab.com/mnielsen/issue-bot): Small Go program that runs in CI to automatically create issues when pipelines fail, saving developers from doing it manually.
- [reviews-bot](https://gitlab.com/mnielsen/reviews-bot): Generates a Markdown table of Merge Requests ready for review.
- [gitlab-ci-go](https://gitlab.com/mnielsen/gitlab-ci-go): Write GitLab CI pipelines in Golang.
- [dotfiles](https://gitlab.com/mitchenielsen/dotfiles): The configuration I use for my development environment (`neovim`, `tmux`, etc.)
